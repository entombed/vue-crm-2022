Небольшой проект домашней **CRM** системы ведения учета доходов и расходов

![](./crm-vue-2022.png)
### [live demo](https://vue-crm-2022-c43c6.web.app)

Реализовано на *VueJs 3* с использование сторонних компонентов:

Пагинации [link](https://github.com/HENNGE/vue3-pagination)
```
@hennge/vue3-pagination
```
Валидация форм
[link](https://vuelidate.js.org/)
```
@vuelidate
```
Построения диограм [link](https://vue-chartjs.org/)
```
vue-chartjs
```
Хранение данных, создание и авторизация пользователей [link](https://firebase.google.com/)
```
firebase
```
CSS стили и элементы интерфейса [link](https://materializecss.com)
```
materialize-css
```
Вспомогательны функции для работы с массивами [link](https://lodash.com/)
```
lodash
```

## Cборка проекта
```
npm install
```
## Запуск проекта
```
npm run serve
```

## Пользователь для входа
```
email: user@crm.local
password: 123456
```
