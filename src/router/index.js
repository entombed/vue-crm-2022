import { getAuth } from 'firebase/auth';
import { createRouter, createWebHistory } from 'vue-router';

const routes = [
  {
    path: '/login',
    name: 'login',
    meta: { layout: 'empty' },
    component: () => import('../views/LoginView.vue'),
  },
  {
    path: '/register',
    name: 'register',
    meta: { layout: 'empty' },
    component: () => import('../views/RegisterView.vue'),
  },
  {
    path: '/',
    name: 'home',
    meta: { layout: 'main', protected: true },
    component: () => import('../views/HomeView.vue'),
  },
  {
    path: '/categories',
    name: 'categories',
    meta: { layout: 'main', protected: true },
    component: () => import('../views/CategoriesView.vue'),
  },
  {
    path: '/detail/:id',
    name: 'detail',
    meta: { layout: 'main', protected: true },
    component: () => import('../views/DetailView.vue'),
  },
  {
    path: '/history',
    name: 'history',
    meta: { layout: 'main', protected: true },
    component: () => import('../views/HistoryView.vue'),
  },
  {
    path: '/planning',
    name: 'planning',
    meta: { layout: 'main', protected: true },
    component: () => import('../views/PlanningView.vue'),
  },
  {
    path: '/profile',
    name: 'profile',
    meta: { layout: 'main', protected: true },
    component: () => import('../views/ProfileView.vue'),
  },
  {
    path: '/record',
    name: 'record',
    meta: { layout: 'main', protected: true },
    component: () => import('../views/RecordView.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

router.beforeEach((to, from, next) => {
  const isProtected = to.matched.some((record) => record.meta.protected);
  const isAuth = getAuth().currentUser;
  if (!isProtected) {
    next();
    return;
  }
  if (isProtected && isAuth) {
    next();
    return;
  }
  next('/login?messge=login');
});

export default router;
