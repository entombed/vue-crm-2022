export default function currencyFilter(value, currency = 'UAH') {
  return new Intl.NumberFormat(currency, {
    style: 'currency',
    currency: currency,
  }).format(value);
}
