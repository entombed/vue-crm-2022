import { createApp } from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import message from '@/utils/message.plugin';
import LoaderSnippet from '@/components/LoaderSnippet';
import directives from '@/directives';
import 'materialize-css/dist/js/materialize.min.js';

import { initializeApp } from 'firebase/app';
import { getAuth, onAuthStateChanged } from 'firebase/auth';

import VPagination from '@hennge/vue3-pagination';
import '@hennge/vue3-pagination/dist/vue3-pagination.css';

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyA20iUzx3c1f_raJXIfnOE8YBzVf1fEwRg',
  authDomain: 'vue-crm-2022-c43c6.firebaseapp.com',
  projectId: 'vue-crm-2022-c43c6',
  storageBucket: 'vue-crm-2022-c43c6.appspot.com',
  messagingSenderId: '1081099601571',
  appId: '1:1081099601571:web:8b8a8733de284b825aa116',
  databaseURL:
    'https://vue-crm-2022-c43c6-default-rtdb.europe-west1.firebasedatabase.app',
};

// Initialize Firebase
initializeApp(firebaseConfig);

let vue = null;
const auth = getAuth();
onAuthStateChanged(auth, () => {
  if (!vue) {
    vue = createApp(App);
    vue.component(LoaderSnippet.name, LoaderSnippet);
    vue.component('vue3-pagination', VPagination);
    directives.forEach((d) => {
      vue.directive(d.name, d);
    });
    vue.use(message);
    vue.use(store).use(router).mount('#app');
  }
});
