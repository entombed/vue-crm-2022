import { getDatabase, ref, push, child, update, get } from 'firebase/database';

const getPath = (uid) => {
  return `users/${uid}/categories`;
};

export default {
  state: {
    categories: [],
    selectedCategoryId: null,
  },
  getters: {
    selectedCategoryId: (s) => s.selectedCategoryId,
  },
  mutations: {
    setCategories(state, categories) {
      state.categories = categories;
    },
    setSelectedCategoryId(state, id) {
      state.selectedCategoryId = id;
    },
  },
  actions: {
    async updateCategory({ commit, dispatch }, { limit, name, id }) {
      try {
        const db = getDatabase();
        const uid = await dispatch('getUid');
        const updates = {};
        updates[`${getPath(uid)}/${id}`] = { name, limit };
        await update(ref(db), updates);
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },

    async fetchCaregories({ commit, dispatch }) {
      try {
        const db = getDatabase();
        const uid = await dispatch('getUid');
        const responce = await get(child(ref(db), getPath(uid)));
        const result = responce.val() ? responce.val() : [];
        const categories = Object.keys(result).map((key) => {
          return { ...result[key], id: key };
        });
        return categories;
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },

    async fetchCaregoryById({ commit, dispatch }, id) {
      try {
        const db = getDatabase();
        const uid = await dispatch('getUid');
        const path = getPath(uid);
        const responce = await get(child(ref(db), `${path}/${id}`));
        return { ...responce.val(), id };
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },

    async createCategory({ commit, dispatch }, { name, limit }) {
      try {
        const db = getDatabase();
        const uid = await dispatch('getUid');
        const id = await push(child(ref(db), getPath(uid))).key;
        const updates = {};
        updates[`${getPath(uid)}/${id}`] = { name, limit };
        await update(ref(db), updates);
        return { name, limit, id };
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
  },
};
