import { createStore } from 'vuex';
import auth from './auth.store';
import info from './info.store';
import fixer from './fixer.store';
import category from './category.store';
import record from './record.store';

export default createStore({
  state: {
    error: null,
    currencies: ['UAH', 'USD', 'EUR'],
  },
  getters: {
    error: (s) => s.error,
    currencies: (s) => s.currencies,
  },
  mutations: {
    setError(state, error) {
      state.error = error;
    },
    clearError(state) {
      state.error = null;
    },
  },
  actions: {
    async getCurrenciesString({ getters }) {
      return getters.currencies.join();
    },
  },
  modules: {
    auth,
    info,
    fixer,
    category,
    record,
  },
});
