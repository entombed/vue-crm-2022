export default {
  state: {
    mockCurrency: {
      success: true,
      timestamp: 1647599284,
      base: 'EUR',
      date: '2022-03-18',
      rates: {
        UAH: 32.612812,
        USD: 1.105107,
        EUR: 1,
      },
    },
  },
  getters: {
    mockCurrency: (s) => s.mockCurrency,
  },
  actions: {
    async fetchFixerCurrecy({ dispatch, getters }) {
      try {
        const currencies = await dispatch('getCurrenciesString');
        const key = process.env.VUE_APP_FIXERAPIKEY;
        const url = `https://data.fixer.io/api/latest?access_key=${key}&symbols=${currencies}`;
        const result = await fetch(url);
        let parsedResult = await result.json();
        if (
          parsedResult &&
          'success' in parsedResult &&
          parsedResult.success === false
        ) {
          console.error(parsedResult);
          parsedResult = getters.mockCurrency;
        }
        return parsedResult;
      } catch (error) {
        throw new Error(error);
      }
    },
  },
};
