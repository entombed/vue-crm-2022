import { getDatabase, onValue, ref, update } from 'firebase/database';

export default {
  state: {
    info: {},
  },
  mutations: {
    setInfoUser(state, info) {
      state.info = info;
    },
    clearInfoUser(state) {
      state.info = {};
    },
  },
  actions: {
    async fetchInfoUser({ dispatch, commit }) {
      try {
        const uid = await dispatch('getUid');
        const db = getDatabase();
        await onValue(
          ref(db, `/users/${uid}/info`),
          (data) => {
            const info = data.val();
            commit('setInfoUser', info);
          },
          {
            onlyOnce: true,
          }
        );
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },

    async updateInfo({ dispatch, commit, getters }, infoDate) {
      try {
        const db = getDatabase();
        const uid = await dispatch('getUid');
        const info = {
          ...getters.info,
          ...infoDate,
        };
        const updates = {};
        updates[`users/${uid}/info`] = info;
        await update(ref(db), updates);
        dispatch('fetchInfoUser');
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
  },
  getters: {
    info: (s) => s.info,
  },
};
