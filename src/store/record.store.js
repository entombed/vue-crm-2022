import { getDatabase, ref, update, get, child, push } from 'firebase/database';

const getPath = (uid) => {
  return `users/${uid}/records`;
};

export default {
  actions: {
    async createRecord({ commit, dispatch }, recordDate) {
      try {
        const db = getDatabase();
        const uid = await dispatch('getUid');
        const path = getPath(uid);
        const id = await push(child(ref(db), path)).key;
        const updates = {};
        updates[`${path}/${id}`] = recordDate;
        await update(ref(db), updates);
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },

    async fetchRecords({ commit, dispatch }) {
      try {
        const db = getDatabase();
        const uid = await dispatch('getUid');
        const path = getPath(uid);
        const responce = await get(child(ref(db), path));
        const result = responce.val() ? responce.val() : [];
        const records = Object.keys(result).map((key) => {
          return { ...result[key], id: key };
        });
        return records;
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
    async fetchRecordById({ commit, dispatch }, id) {
      try {
        const db = getDatabase();
        const uid = await dispatch('getUid');
        const path = getPath(uid);
        const responce = await get(child(ref(db), `${path}/${id}`));
        return { ...responce.val(), id };
      } catch (error) {
        commit('setError', error);
        throw error;
      }
    },
  },
};
