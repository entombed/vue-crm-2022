import {
  getAuth,
  signInWithEmailAndPassword,
  signOut,
  createUserWithEmailAndPassword,
} from 'firebase/auth';
import { getDatabase, ref, set } from 'firebase/database';
export default {
  actions: {
    async login({ commit }, { email, password }) {
      try {
        const auth = getAuth();
        await signInWithEmailAndPassword(auth, email, password);
      } catch (error) {
        commit('setError', error);
      }
    },

    async logout({ commit }) {
      try {
        const auth = getAuth();
        await signOut(auth);
        commit('clearInfoUser');
      } catch (error) {
        commit('setError', error);
      }
    },

    getUid() {
      const userDate = getAuth().currentUser;
      return userDate ? userDate.uid : null;
    },

    // eslint-disable-next-line no-empty-pattern
    async register({ dispatch, commit }, { email, password, name }) {
      try {
        const auth = getAuth();
        await createUserWithEmailAndPassword(auth, email, password);
        const uid = await dispatch('getUid');
        const db = getDatabase();
        await set(ref(db, `/users/${uid}/info`), {
          bill: 10000,
          name: name,
          locale: 'ru-RU',
        });
        dispatch('createCategory', { limit: 10, name: 'Default' });
      } catch (error) {
        commit('setError', error);
      }
    },
  },
};
