import _ from 'lodash';

export default {
  data: () => ({
    page: 1,
    numberAllPages: 1,
    countRecordsOnPage: 5,
    allRecords: [],
    onPageRecords: [],
  }),
  mounted() {
    if (this.$route.query && 'page' in this.$route.query) {
      this.page = +this.$route.query.page;
    }
  },
  methods: {
    setupPagination(records) {
      this.allRecords = _.chunk(records, this.countRecordsOnPage);
      this.numberAllPages = _.size(this.allRecords);
      this.onPageRecords = this.allRecords[this.page - 1] || this.allRecords[0];
    },
    onClickPagination(page) {
      this.$router.push(`${this.$route.path}?page=${page}`);
      this.onPageRecords = this.allRecords[page - 1];
    },
  },
};
