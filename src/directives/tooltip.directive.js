/* eslint-disable no-undef */
export default {
  name: 'tooltip',
  mounted(element, binding) {
    const { html, position = 'bottom' } = binding.value;
    M.Tooltip.init(element, { html, position });
  },
  beforeUnmount(element) {
    const tooltip = M.Tooltip.getInstance(element);
    if (tooltip && 'destroy' in tooltip) {
      tooltip.destroy();
    }
  },
};
