/* eslint-disable no-undef */
export default {
  // eslint-disable-next-line no-unused-vars
  install(app, options) {
    app.config.globalProperties.$showMessage = function (
      html,
      isError = false
    ) {
      html = isError ? `[Ошибка]: ${html}` : html;
      M.toast({ html });
    };
  },
};
