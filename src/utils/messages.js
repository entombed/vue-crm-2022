/* eslint-disable prettier/prettier */
export default {
  'logout': 'Вы вышли из системы',
  'login': 'Необходима авторизация',
  'auth/user-not-found': 'Пользоватеь не найден',
  'auth/wrong-password': 'Не верный пароль',
  'undefined_error': 'Не установленная ошибка',
  'auth/email-already-in-use': 'Пользователь существует',
  'category_created': 'Категория успешно создана',
  'category_updated': 'Категория обновлена',
  'add_category': 'Добавить категорию'
};
